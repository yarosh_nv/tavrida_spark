import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, FileUtil, Path}
import org.apache.spark.sql.functions.{col, concat, lit, substring}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

import java.io.File
import scala.util.Try

object CsvToDataframeScalaApp extends App {
  val sourcePath = "./src/main/resources/source/"
  val resultPath = "./src/main/resources/result/РСД1_ТКП.csv"

  val spark = SparkSession.builder
    .appName("CSV to Dataset")
    .master("local[*]")
    .getOrCreate

  def createDataFrame(filePath: String): Try[DataFrame] = Try {
    spark.read
      .format("csv")
      .option("inferSchema", value = true)
      .option("header", "true")
      .load(filePath)
  }

  val accountDF = createDataFrame(sourcePath + "account.csv").get
  val account_typeDF = createDataFrame(sourcePath + "account_type.csv").get
  val account_balance_historyDF = createDataFrame(sourcePath + "account_balance_history.csv").get
  val contractDF = createDataFrame(sourcePath + "contract.csv").get
  val subjDF = createDataFrame(sourcePath + "subj.csv").get
  val subj_indidfnDF = createDataFrame(sourcePath + "subj_indidfn.csv").get

  def accountJoinAccountType = accountDF
    .join(account_typeDF)
    .filter(accountDF("account_type_id") === account_typeDF("id"))
    .filter(col("code") === "OVERDUE_INTEREST")
    .drop(account_typeDF("id"))

  def accountJoinAccountBalance = accountJoinAccountType
    .join(account_balance_historyDF)
    .filter(account_balance_historyDF("account_id") === accountJoinAccountType("id"))
    .filter(col("turn_debit") =!= 0 || col("turn_credit") =!= 0)
    .filter(col("oper_date").between("2021-01-01","2022-01-01"))
    .drop(accountJoinAccountType("id"))

  def accountJoinContract = accountJoinAccountBalance
    .join(contractDF)
    .filter(contractDF("id") === accountJoinAccountBalance("contract_id"))
    .drop(contractDF("id"))

  def subjJoinIndidfn = subjDF
    .join(subj_indidfnDF, subjDF("subj_sk") === subj_indidfnDF("subj_sk") && subj_indidfnDF("active") === 1, "left")
    .drop(subj_indidfnDF("subj_sk"))

  def contractJoinSubj = accountJoinContract
    .join(subjJoinIndidfn, subjJoinIndidfn("sid") === concat(lit("ind|Individual_"), accountJoinContract("client_id") ), "left")
    .drop(subjJoinIndidfn("sid"))

  def result = contractJoinSubj
    .select(
      col("oper_date").as("operation_date"),
      col("client_id").as("subject_sid_ek"),
      concat(col("family_name"), lit(" "), col("first_name"), lit(" "), col("father_name")).as("subject_name"),
      col("date_open").as("agreement_date"),
      col("contract_number").as("agreement_number"),
      substring(col("account_number"), 1, 5).as("balance_account"),
      substring(col("account_number"), 6, 3).as("account_currency"),
      col("turn_debit"),
      col("turn_credit"),
      col("contract_id").as("agreement_id_ek"),
      col("tb_number"),
      col("subdivision_code")
    )
  result.show

  result
    .repartition(1)
    .write
    .mode(SaveMode.Overwrite)
    .option("header", "true")
    .csv("./src/main/resources/data")

  if(new File(resultPath).exists()){
    new File(resultPath).delete()
  }

  val hadoopConfig = new Configuration()
  val hdfs = FileSystem.get(hadoopConfig)
  val srcFilePath=new Path("./src/main/resources/data")
  val destFilePath= new Path(resultPath)
  FileUtil.copyMerge(hdfs, srcFilePath, hdfs, destFilePath, true, hadoopConfig, null)

  println(s"Table saved in $resultPath")
  spark.stop
}